package com.greatlearning.indiaBank;

import java.util.Objects;

public class Customer {

    String bankAccountNo;
    String password;

    public Customer(String bankAccountNo, String password) {
        this.bankAccountNo = bankAccountNo;
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(bankAccountNo, customer.bankAccountNo) && Objects.equals(password, customer.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bankAccountNo, password);
    }
}
