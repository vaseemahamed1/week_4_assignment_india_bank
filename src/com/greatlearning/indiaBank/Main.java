package com.greatlearning.indiaBank;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner input = new Scanner(System.in);
        Integer balanceAmount = 0;
        boolean exit = false;

        File file = new File("transaction.txt");
        file.createNewFile();

        // hard coded customer list with bank acc and password
        List<Customer> customerList = new ArrayList<>();
        customerList.add(new Customer("11111", "pass1"));
        customerList.add(new Customer("22222", "pass2"));

        //login process
        performLoginOperation(input, false, customerList);

        do {
            System.out.printf("\nEnter operation to perform\n 1. Deposit \n 2. WithDraw \n 3. Transfer \n 0. Logout \n");
            System.out.printf("--------------------------------------------\n");

            String choice = input.nextLine();

            switch (choice) {
                case "1": {
                    Bank bank = (balance) -> {
                        System.out.printf("enter the amount you want to deposit\n");
                        String deposit = input.nextLine();
                        balance = balance + Integer.parseInt(deposit);
                        System.out.printf("amount %s deposited successfully, current balance: %s\n", deposit, balance);
                        logTransactionToFile(String.format("amount %s deposit transaction, current balance %s", deposit, balance));
                        return balance;
                    };
                    balanceAmount = bank.performTransaction(balanceAmount);
                    break;
                }
                case "2": {
                    Bank bank = (balance) -> {
                        System.out.printf("enter the amount you want to withdraw\n");
                        String withdraw = input.nextLine();
                        balance = balance - Integer.parseInt(withdraw);
                        System.out.printf("amount %s withdrawal successfully, current balance: %s\n", withdraw, balance);
                        logTransactionToFile(String.format("amount %s withdrawal transaction, current balance %s", withdraw, balance));
                        return balance;
                    };
                    balanceAmount = bank.performTransaction(balanceAmount);
                    break;
                }
                case "3": {
                    Bank bank = (balance) -> {
                        System.out.printf("enter the otp\n");
                        String otp = String.format("%04d", new Random().nextInt(10000));
                        System.out.printf("%s\n", otp);
                        String enteredOtp = input.nextLine();

                        if (otp.equals(enteredOtp)) {
                            System.out.printf("otp verification successful !!!\n");
                            System.out.printf("  enter the amount and Bank Account no to which you want to transfer \n");
                            String amount = input.nextLine();
                            String bankAccNo = input.nextLine();
                            balance = balance - Integer.parseInt(amount);
                            System.out.printf("amount %s transferred successful to bankAccount %s, Final balance \n", amount, bankAccNo, balance);
                            logTransactionToFile(String.format("amount %s transfer transaction, current balance %s", amount, balance));
                        } else {
                            System.out.printf("otp verification failed !!!\n");
                        }
                        return balance;
                    };
                    balanceAmount = bank.performTransaction(balanceAmount);
                    break;
                }
                case "0":
                    System.out.printf("logging out\n");
                    exit = true;
                    break;
                default:
                    System.out.printf("invalid choice\n");
            }
        } while (!exit);


    }

    private static void performLoginOperation(Scanner input, Boolean loggedIn, List<Customer> customerList) {
        System.out.printf("\nWelcome to Login page \n\n");
        do {
            System.out.printf("enter the bank Account no: \n");
            String accNo = input.nextLine();

            System.out.printf("enter the password for the corresponding bank account no: \n");
            String password = input.nextLine();

            if (customerList.contains(new Customer(accNo, password))) {
                System.out.printf("\n !!!! Welcome to India Bank !!!! \n");
                System.out.printf("--------------------------------------------\n");
                loggedIn = true;
            } else {
                System.out.printf("\nInvalid credential, Try again\n");
            }

        } while (!loggedIn);
    }

    private static void logTransactionToFile(String message) throws IOException {
        FileWriter writer = new FileWriter("transaction.txt", true);
        writer.append('\n');
        writer.write(message);
        writer.close();
    }
}
