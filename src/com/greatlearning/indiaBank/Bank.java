package com.greatlearning.indiaBank;

import java.io.IOException;

@FunctionalInterface
public interface Bank {
    Integer performTransaction(Integer balance) throws IOException;
}
